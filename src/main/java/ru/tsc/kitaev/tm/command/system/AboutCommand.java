package ru.tsc.kitaev.tm.command.system;

import ru.tsc.kitaev.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info...";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Danil Kitaev");
        System.out.println("E-MAIL: dkitaev@tsconsulting.com");
    }

}
