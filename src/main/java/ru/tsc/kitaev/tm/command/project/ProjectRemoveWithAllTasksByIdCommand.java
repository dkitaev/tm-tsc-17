package ru.tsc.kitaev.tm.command.project;

import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public class ProjectRemoveWithAllTasksByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(projectId) == null) throw new ProjectNotFoundException();
        final Project taskUpdated = serviceLocator.getProjectTaskService().removeById(projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

}
